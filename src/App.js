import React from 'react';
import ColorBoxes from './Components/ColorBoxes/ColorBoxes';
import './App.css';

function App() {
  return (
    <div className="App">
      <ColorBoxes />
    </div>
  );
}

export default App;
