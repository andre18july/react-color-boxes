import React from 'react';
import './Box.css';

const box = props => {

    return(
        <div style={{backgroundColor: props.color}} onClick={props.click} className="Box"></div>
    )
    
}

export default box;