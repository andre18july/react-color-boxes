import React, { Component } from 'react';
import './ColorBoxes.css';
import Box from '../Box/Box';
import { getRandomColor } from '../../Helpers/RandomColor';

class ColorBoxes extends Component {

    static defaultProps = {
        numBoxes: 18,
        colors: [
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor(),
            getRandomColor()
        ]
    }

    constructor(props){
        super(props);

        this.state = {
            boxes: Array.from({length: this.props.numBoxes})
        }
        
    }

    componentWillMount(){
        const newBoxes = [...this.state.boxes];
        for(let i = 0; i < newBoxes.length; i++){
            const newColor = this.props.colors[Math.floor(Math.random() * this.props.colors.length)];
            newBoxes[i] = newColor;
        }
        
        this.setState({
            boxes: newBoxes
        })
    }


    handlerClickBox = e => {
        const newColor = this.props.colors[Math.floor(Math.random() * this.props.colors.length)];
        e.target.style.backgroundColor = newColor;

    }

    render(){

        const boxes = this.state.boxes.map(box => <Box click={this.handlerClickBox} color={box}/>)      

        return(
            <div>
                <h1>Color Boxes</h1>
                <div className="ColorBoxes">
                    {boxes}
                </div>
            </div>
        );
    }
}

export default ColorBoxes;